package universite.angers.master.info.client.bomberman.controller;

import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.Game;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.FactoryBombermanGame;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.client.bomberman.view.ViewBomberman;

/**
 * Controlleur qui permet d'assurer les contrôles du jeu quand des actions ont
 * été effectuées par l’utilisateur dans la Vue (par exemple lorsque
 * l’utilisateur clique sur un bouton)
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ControllerBombermanGame extends Controller {

	private static final Logger LOG = Logger.getLogger(ControllerBombermanGame.class);

	/**
	 * La vue bomberman
	 */
	private ViewBomberman viewBomberman;

	public ControllerBombermanGame(Game game) {
		super(game);
	}

	@Override
	public void displayGameFinish() {

		boolean gameOver = this.game.isGameOver();
		LOG.debug("Game over : " + gameOver);
		
		this.close();
		
		if (gameOver) {
			this.gameOver();
		} else {
			this.gameWin();
		}
	}
	
	private void gameOver() {
		Object[] options = { "OK", "Quitter" };

		int choix = JOptionPane.showOptionDialog(
				null, 
				"Voulez-vous réessayer ?", 
				"Game Over", 
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE, 
				null, 
				options, 
				options[0]);
		
		LOG.debug("Choix : " + choix);
		LOG.debug("Continuer : " + JOptionPane.OK_OPTION);
		LOG.debug("Quitter : " + JOptionPane.CANCEL_OPTION);
		
		if(choix == JOptionPane.OK_OPTION) {
			//Si le joueur veut continuer, dans ce cas on réinitialise le jeu
			this.open();
		} 
		else {
			//Sinon le joueur est déconnecté
			this.logout();
		}
	}
	
	/**
	 * Affiche partie gagné
	 */
	private void gameWin() {
		Object[] options = { "Passer au niveau suivant", "Quitter" };

		int choix = JOptionPane.showOptionDialog(
				null,
				"Félicitation vous avez gagné. Voulez-vous passer au niveau suivant ?", 
				"Partie terminée",
				JOptionPane.OK_CANCEL_OPTION, 
				JOptionPane.QUESTION_MESSAGE, 
				null, 
				options, 
				options[0]);
		
		LOG.debug("Choix : " + choix);
		LOG.debug("Continuer : " + JOptionPane.OK_OPTION);
		LOG.debug("Quitter : " + JOptionPane.CANCEL_OPTION);

		if(choix == JOptionPane.OK_OPTION) {
			//Si le joueur veut continuer, dans ce cas on réinitialise le jeu
			this.open();
		} 
		else {
			//Sinon le joueur est déconnecté
			this.logout();
		}
	}

	@Override
	public void login(String login, String password) {
		LOG.debug("Login : " + login);
		LOG.debug("Password : " + password);

		// On ouvre le client
		boolean open = ClientBombermanGame.getInstance().getClient().open();
		LOG.debug("Client open : " + open);

		if (!open)
			return;

		// On enregistre la demande d'identification au serveur
		boolean register = ClientBombermanGame.getInstance().registerRequestLogin(login, password);
		LOG.debug("Register login : " + register);

		if (!register)
			return;

		// On attend de recevoir la réponse du serveur
		LOG.debug("Wait login...");
		ClientBombermanGame.getInstance().getClient().waitFinish();
		LOG.debug("Wait login is done");

		// On récupère la réponse contenu dans le joueur
		Player player = ClientBombermanGame.getInstance().getPlayer();
		LOG.debug("Client player : " + player);

		if (player == null)
			return;

		// Si le joueur est bel et bien enregistré, on initialise le jeu
		if (player.isRecord()) {

			//On initialise la map
			String filename = player.getPathMap();
			LOG.debug("File name map : " + filename);
			
			Map.getInstance().setFilename(filename);
			
			//On ouvre le plateau
			this.open();
			
			//On affiche le message du serveur
			String message = player.getMessages().get(0);
			LOG.debug("Message : " + message);
			
			this.viewBomberman.writeMessage(message);

		} else {
			Object[] optionsFin = { "OK" };

			JOptionPane.showOptionDialog(null, player.getMessages().get(0), "Identification échouée",
					JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionsFin, optionsFin[0]);

			this.logout();
		}
	}

	@Override
	public void logout() {
		// On enregistre la demande de se déconnecter
		boolean logout = ClientBombermanGame.getInstance().registerRequestLogout();
		LOG.debug("Register logout : " + logout);

		if (!logout)
			return;

		// On attend de recevoir la réponse du serveur
		LOG.debug("Wait logout...");
		ClientBombermanGame.getInstance().getClient().waitFinish();
		LOG.debug("Wait logout is done");

		boolean close = ClientBombermanGame.getInstance().getClient().close();
		LOG.debug("Client close : " + close);

		System.exit(0);
	}

	@Override
	public void init() {
		//Il faut réinstancier le game à cause de la vue qui a du mal à se réinitialiser.
		//Comme les vues sont enregistrés en tant que observer dans le game, les références des vues ne sont pas totalement
		//supprimés lorsqu'on fait dispose() dans les vues
		//En supprimant le game, toutes les références enregistrés des vues sont bel et bien supprimés par le garbage collector.
		this.game = FactoryBombermanGame.getBombermanGameForPlayer(1000, 1000);
		this.game.init();
	}

	@Override
	public void start() {
		// On affiche la vue
		if(this.viewBomberman == null) {
			this.viewBomberman = new ViewBomberman(this, this.game);	
		}
	}

	@Override
	public void step() {
		this.game.step();
	}

	@Override
	public void run() {
		this.game.launch();
	}

	@Override
	public void stop() {
		this.game.stop();
	}

	@Override
	public void setTime(long time) {
		this.game.setTime(time * 1000);
	}

	@Override
	public void open() {		
		// On charge la map
		this.init();

		// On charge la vue
		this.start();

		// On charge le jeu
		this.run();
	}

	@Override
	public void close() {
		this.game.close();
		
		// On ferme la fenetre si déjà active
		if (this.viewBomberman != null) {
			this.viewBomberman.close();
			this.viewBomberman = null;
		}
	}

	/**
	 * Méthode qui permet d'effectuer une action sur un agent bomberman
	 * 
	 * @param action l'action a effectuer
	 */
	public void doAction(AgentAction action) {
		Map.getInstance().getCurrentAgentKind().doAction(action);
	}

	/**
	 * Méthode qui permet d'effectuer un mouvement sur un agent bomberman
	 * 
	 * @param move le mouvement à effectuer
	 */
	public void doMove(AgentMove move) {
		Map.getInstance().getCurrentAgentKind().doMove(move);
	}

	/**
	 * @return the viewBomberman
	 */
	public ViewBomberman getViewBomberman() {
		return viewBomberman;
	}

	/**
	 * @param viewBomberman the viewBomberman to set
	 */
	public void setViewBomberman(ViewBomberman viewBomberman) {
		this.viewBomberman = viewBomberman;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((viewBomberman == null) ? 0 : viewBomberman.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ControllerBombermanGame other = (ControllerBombermanGame) obj;
		if (viewBomberman == null) {
			if (other.viewBomberman != null)
				return false;
		} else if (!viewBomberman.equals(other.viewBomberman))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ControllerBombermanGame [viewBomberman=" + viewBomberman + "]";
	}
}
