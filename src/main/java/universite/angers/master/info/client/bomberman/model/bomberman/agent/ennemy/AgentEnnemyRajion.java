package universite.angers.master.info.client.bomberman.model.bomberman.agent.ennemy;

import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;

/**
 * Source : https://bomberman.fandom.com/wiki/Rajion Rajion constantly pursues
 * Bomberman and will harm the player on collision. Rajion also pauses
 * momentarily and shoots electricity to stun the player and exhausts itself
 * after before resuming
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentEnnemyRajion extends AgentEnnemy {

	private static final long serialVersionUID = 1L;

	public AgentEnnemyRajion(String identifiant, String name, int x, int y, AgentMove move, char type, ColorAgent color,
			boolean isInvincible, boolean isSick) {
		super(identifiant, name, x, y, move, type, color, isInvincible, isSick);
	}

	public AgentEnnemyRajion(InfoAgent infoAgent) {
		super(infoAgent);
	}
}
