package universite.angers.master.info.client.bomberman.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.controller.Controller;
import universite.angers.master.info.client.bomberman.controller.ControllerBombermanGame;
import universite.angers.master.info.client.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.Game;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ItemType;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;

/**
 * Vue qui affiche le plateau de bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ViewBomberman extends View implements KeyListener {

	private static final Logger LOG = Logger.getLogger(ViewBomberman.class);
	private static final long serialVersionUID = 1L;

	private PanelBomberman panelBomberman;
	private JLabel lifeValeur;
	private JLabel fireValeur;
	private JLabel bombValeur;
	private JLabel speedValeur;
	private JLabel pointValeur;
	private JPanel chat;
	private JTextArea viewChat;
	private JTextField writeChat;

	public ViewBomberman(Controller controller, Game game) {
		super(controller, game);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		this.updateCaracteristiqueBomberman();
		this.repaint();
		
		boolean finish = (boolean) arg1;
		System.out.println("Turn");
		LOG.debug("Finish : " + finish);
		
		if (finish) {
			this.controller.displayGameFinish();
		}
	}

	@Override
	protected void createFrame() {
		// La fenetre
		this.setTitle("Bomberman");
		this.setSize(new Dimension(1280, 720));
		Dimension windowSize = this.getSize();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point centerPoint = ge.getCenterPoint();
		int dx = centerPoint.x - windowSize.width / 2;
		int dy = centerPoint.y - windowSize.height / 2 - 640;
		this.setLocation(dx, dy);
		this.setLocationRelativeTo(null); // permet de centrer la fenetre au centre de l'écran

		// Le plateau
		this.panelBomberman = new PanelBomberman();
		
		JPanel base = new JPanel();
		base.setLayout(new BorderLayout());

		/**
		 * Chat qui permet de notifier les joueurs
		 */
		this.chat = new JPanel();
		this.chat.setLayout(new BorderLayout());
		this.chat.setSize(1280, 150);
		this.chat.setPreferredSize(new Dimension(1280,100));;
		this.viewChat = new JTextArea(3, 1);
		this.viewChat.setPreferredSize(new Dimension(1280,80));
		this.viewChat.setEditable(false);
		this.writeChat = new JTextField();
		this.chat.add(this.viewChat, BorderLayout.NORTH);
		this.chat.add(this.writeChat, BorderLayout.SOUTH);

		/**
		 * Information permettant d'afficher l'état du Bomberman
		 */
		JPanel information = new JPanel();

		JLabel life = new JLabel("Life : ");
		lifeValeur = new JLabel();

		ImageIcon icon = new ImageIcon("./image/piques.png");
		JLabel piques1 = new JLabel(icon);
		JLabel piques2 = new JLabel(icon);
		JLabel piques3 = new JLabel(icon);
		JLabel piques4 = new JLabel(icon);

		JLabel fire = new JLabel("Fire : ");
		fireValeur = new JLabel();

		JLabel bomb = new JLabel("Bomb : ");
		bombValeur = new JLabel();

		JLabel speed = new JLabel("Speed : ");
		speedValeur = new JLabel();

		JLabel attack = new JLabel("Point : ");
		pointValeur = new JLabel();

		information.add(life);
		information.add(lifeValeur);
		information.add(piques1);
		information.add(fire);
		information.add(fireValeur);
		information.add(piques2);
		information.add(bomb);
		information.add(bombValeur);
		information.add(piques3);
		information.add(speed);
		information.add(speedValeur);
		information.add(piques4);
		information.add(attack);
		information.add(pointValeur);

		base.add(panelBomberman, BorderLayout.CENTER);

		base.add(information, BorderLayout.NORTH);

		base.add(chat, BorderLayout.SOUTH);

		this.setContentPane(base);
		this.addKeyListener(this);
		this.setVisible(true);
		
		this.refresh();
	}


	/**
	 * Mettre à jour les caractéristiques du Bomberman
	 */
	private void updateCaracteristiqueBomberman() {
		this.lifeValeur.setText(
				"" + Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.LIFE).getCapacityActualItem());
		this.fireValeur.setText(
				"" + Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.FIRE).getCapacityActualItem());
		this.bombValeur.setText(
				"" + Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.BOMB).getCapacityActualItem());
		this.speedValeur.setText(
				"" + Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.SPEED).getCapacityActualItem());
		this.pointValeur.setText(
				"" + Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.POINT).getCapacityActualItem());
	}
	
	/**
	 * Rafraichie le plateau
	 */
	private void refresh() {
		this.updateCaracteristiqueBomberman();
		this.repaint();
		this.panelBomberman.requestFocus();
		this.requestFocus();
	}
	
	/**
	 * Ecrire un message dans le chat
	 * @param msg
	 */
	public void writeMessage(String msg) {
		String oldMessages = this.viewChat.getText();
		String[] messages = oldMessages.split(System.lineSeparator());
		String newMessage = "";
		if(messages.length > 4) {
			messages[0]=messages[1];
			messages[1]=messages[2];
			messages[2]=messages[3];
			messages[3] = msg;
			for (String m : messages) {
				newMessage += m + System.lineSeparator();
			}
		}else {
			newMessage = oldMessages + System.lineSeparator() + msg;
		}
		
		this.viewChat.setText(newMessage);
		
		this.refresh();
	}

	/**
	 * Effectuer les déplacement sur les bomberman
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			((ControllerBombermanGame) controller).doMove(AgentMove.MOVE_UP);
			break;
		case KeyEvent.VK_DOWN:
			((ControllerBombermanGame) controller).doMove(AgentMove.MOVE_DOWN);
			break;
		case KeyEvent.VK_LEFT:
			((ControllerBombermanGame) controller).doMove(AgentMove.MOVE_LEFT);
			break;
		case KeyEvent.VK_RIGHT:
			((ControllerBombermanGame) controller).doMove(AgentMove.MOVE_RIGHT);
			break;
		default:
			((ControllerBombermanGame) controller).doMove(AgentMove.STOP);
			break;
		}
	}
	
	/**
	 * Effectuer les actions que peut réaliser le bomberman
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getKeyChar()) {
		// Poser une bombe
		case 'b':
			((ControllerBombermanGame) controller).doAction(AgentAction.PUT_BOMB);
			break;
		default:
		}
	}
	

	@Override
	public void keyReleased(KeyEvent arg0) {

	}
}
