package universite.angers.master.info.client.bomberman.client.command.request;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet d'envoyer une requete au serveur pour déplacer un agent
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestMoveBomberman implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientRequestMoveBomberman.class);
	
	private Player player;
	
	public ClientRequestMoveBomberman(Player player, AgentMove move) {
		LOG.debug("Request action move player : " + player);
		LOG.debug("Request action move action : " + move);
		
		this.player = player;
		
		this.player.setBomberman(null);
		this.player.setMove(move);
		this.player.setRecord(false);
		this.player.getMessages().clear();
	}
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Request action move player : " + player);
		
		this.player.setBomberman(player.getBomberman());
		this.player.setMove(player.getMove());
		this.player.setRecord(player.isRecord());
		this.player.setMessages(player.getMessages());
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_MOVE_AGENT");
		return this.player;
	}
}