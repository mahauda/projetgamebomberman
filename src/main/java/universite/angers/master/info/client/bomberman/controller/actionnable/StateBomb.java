package universite.angers.master.info.client.bomberman.controller.actionnable;

/**
 * Enumération des états d'une bombe avant d'exploser
 *
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum StateBomb {
	Step1,Step2,Step3,Boom
}