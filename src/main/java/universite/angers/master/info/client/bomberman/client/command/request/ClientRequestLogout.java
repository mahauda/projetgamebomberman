package universite.angers.master.info.client.bomberman.client.command.request;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet d'envoyer une requete au serveur pour s'identifier
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestLogout implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientRequestLogout.class);
	
	private Player player;
	
	public ClientRequestLogout(Player player) {
		LOG.debug("Request logout player : " + player);
		
		this.player = player;
		
		this.player.setRecord(false);
		this.player.getMessages().clear();
	}
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Request logout player : " + player);
		
		this.player.setRecord(player.isRecord());
		this.player.setMessages(player.getMessages());
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_LOGOUT");
		return this.player;
	}
}
