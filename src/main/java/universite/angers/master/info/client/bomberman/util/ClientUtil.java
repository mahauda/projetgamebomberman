package universite.angers.master.info.client.bomberman.util;

/**
 * Classe utilitaire pour le client
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientUtil {

	private ClientUtil() {
		
	}
	
	/**
	 * Vérifier si une chaine est vide
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.trim().isEmpty())
            return false;
        return true;
    }
}
