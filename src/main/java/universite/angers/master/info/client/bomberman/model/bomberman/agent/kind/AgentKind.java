package universite.angers.master.info.client.bomberman.model.bomberman.agent.kind;

import universite.angers.master.info.client.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ItemType;

/**
 * Agent gentil qui peut posséder des bonus ou malus
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class AgentKind extends Agent {

	private static final long serialVersionUID = 1L;

	public AgentKind(String identifiant, String name, int x, int y, AgentMove move, char type, ColorAgent color,
			boolean isInvincible, boolean isSick, StateBomb stateBomb) {
		super(identifiant, name, x, y, move, type, color, isInvincible, isSick);
		this.stateBomb = stateBomb;
	}

	public AgentKind(InfoAgent infoAgent, StateBomb stateBomb) {
		super(infoAgent);
		this.stateBomb = stateBomb;
	}

	/**
	 * Vérifier si le bomberman est en vie Le bomberman est mort lorsqu'il n'a plus
	 * de vie
	 * 
	 * @return
	 */
	public boolean isDead() {
		CapacityItem life = this.getCapacityItem(ItemType.LIFE);
		if (life == null)
			return false;
		else
			return life.getCapacityActualItem() == 0;
	}

	@Override
	public String toString() {
		return "AgentKind [stateBomb=" + stateBomb + ", actions=" + actions + "]";
	}
}
