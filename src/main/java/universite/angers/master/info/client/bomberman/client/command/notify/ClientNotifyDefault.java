package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de recevoir n'importe quelle message par défaut du serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyDefault implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyDefault.class);

	@Override
	public boolean send(Player player) {
		LOG.debug("Client notify default player : " + player);
		
		for(String message : player.getMessages()) {
			LOG.debug(message);
		}
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
