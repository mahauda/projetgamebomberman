package universite.angers.master.info.client.bomberman.client;

import org.apache.log4j.Logger;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.translater.json.TranslaterJsonFactory;
import universite.angers.master.info.api.translater.json.TranslaterJsonToObject;
import universite.angers.master.info.api.translater.json.TranslaterObjectToJson;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyActionPutBomb;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyApplyBonus;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyAttackBomb;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyAttackBomberman;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyDefault;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyGame;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyMoveBomberman;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyMoveEnnemy;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyChat;
import universite.angers.master.info.client.bomberman.client.command.notify.ClientNotifyTurn;
import universite.angers.master.info.client.bomberman.client.command.request.ClientRequestDefault;
import universite.angers.master.info.client.bomberman.client.command.request.ClientRequestLogin;
import universite.angers.master.info.client.bomberman.client.command.request.ClientRequestLogout;
import universite.angers.master.info.client.bomberman.client.command.request.ClientRequestMoveBomberman;
import universite.angers.master.info.client.bomberman.client.command.request.ClientRequestActionPutBomb;
import universite.angers.master.info.client.bomberman.controller.ControllerBombermanGame;
import universite.angers.master.info.client.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.FactoryBombermanGame;
import universite.angers.master.info.network.client.Client;
import universite.angers.master.info.network.client.ClientService;
import universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock;
import universite.angers.master.info.network.server.Subject;

/**
 * Client du jeu Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientBombermanGame {

	private static final Logger LOG = Logger.getLogger(ClientBombermanGame.class);
	
	/**
	 * Le host du client
	 */
	private static final String HOST = "127.0.0.1";
	
	/**
	 * Le port du client
	 */
	private static final int PORT = 2345;
	
	/**
	 * L'unique instance
	 */
	private static ClientBombermanGame instance;
	
	/**
	 * Le client du jeu
	 */
	private Client<Player> client;
	
	/**
	 * Le joueur du jeu
	 */
	private Player player;
	
	/**
	 * Le controlleur du jeu
	 */
	private ControllerBombermanGame controllerBombermanGame;
	
	public static synchronized ClientBombermanGame getInstance() {
		if(instance == null)
			instance = new ClientBombermanGame();
		
		return instance;
	}
	
	private ClientBombermanGame() {		
		this.player = new Player();
		
		//On crée le service
		ClientService<Player> service = new ClientService<>(null, null, new ClientConverterCommandName(), new ReentrantWithoutReadWriteLock(true));	
		LOG.debug("Service client : " + service);
		
		//On enregistre les notifications que peut recevoir le client
		//Ici il s'agit uniquement de faire des sorties consoles quelques soients
		//la notification. Pas de traitement spécifique pour chaque notification
		ClientNotifyDefault notifyDefault = new ClientNotifyDefault();
		ClientNotifyChat notifyChat = new ClientNotifyChat();
		service.registerNotification(Subject.NOTIFY_ECHO.getName(), notifyDefault);
		service.registerNotification(Subject.NOTIFY_CLOSE.getName(), notifyDefault);
        service.registerNotification(Subject.NOTIFY_HELLO.getName(), notifyDefault);
        service.registerNotification(Subject.NOTIFY_NEW.getName(), notifyDefault);
        service.registerNotification("NOTIFY_ACTION_PUT_BOMB", new ClientNotifyActionPutBomb());
        service.registerNotification("NOTIFY_LOGOUT_PLAYER", notifyChat);
        service.registerNotification("NOTIFY_NEW_PLAYER", notifyChat);
        service.registerNotification("NOTIFY_YOUR_TURN", notifyChat);
        service.registerNotification("NOTIFY_PLAYER_TURN", notifyChat);
        service.registerNotification("NOTIFY_MOVE_BOMBERMAN", new ClientNotifyMoveBomberman());
        service.registerNotification("NOTIFY_MOVE_ENNEMY", new ClientNotifyMoveEnnemy());
        service.registerNotification("NOTIFY_ATTACK_BOMBERMAN", new ClientNotifyAttackBomberman());
        service.registerNotification("NOTIFY_ATTACK_BOMB", new ClientNotifyAttackBomb());
        service.registerNotification("NOTIFY_APPLY_BONUS", new ClientNotifyApplyBonus());
        service.registerNotification("NOTIFY_GAME", new ClientNotifyGame());
        service.registerNotification("NOTIFY_TURN", new ClientNotifyTurn());
       
        service.setNotificationDefault(new ClientNotifyDefault());
        service.setRequestDefault(new ClientRequestDefault());

        //On créé les traducteurs pour convertir un objet en json et inversement
        
        //OBJECT --> JSON
        TranslaterObjectToJson<Player> playerToJson = TranslaterJsonFactory.getTranslaterObjectToJson(new TypeToken<Player>() {});
        
        //JSON --> OBJECT
        TranslaterJsonToObject<Player> jsonToPlayer = TranslaterJsonFactory.getTranslaterJsonToObject(new TypeToken<Player>() {});
        
        //On crée le client
        this.client = new Client<>(HOST, PORT, service, playerToJson, jsonToPlayer);
        LOG.debug("Client : " + client);
	}
	
	/**
	 * Méthode qui permet d'initialiser le jeu
	 */
	public void start() {
		this.controllerBombermanGame = new ControllerBombermanGame(FactoryBombermanGame.getBombermanGameForPlayer(1000, 1000));
	}
	
	/**
	 * Requete qui permet de se déconnecter du serveur
	 * @return
	 */
	public boolean registerRequestLogout() {
		return this.client.getClientService().registerRequest("REQUEST_LOGOUT", new ClientRequestLogout(this.player));
	}
	
	/**
	 * Requete qui permet de s'identifier dans le serveur avant de jouer
	 * @return
	 */
	public boolean registerRequestLogin(String login, String password) {
		return this.client.getClientService().registerRequest("REQUEST_LOGIN", new ClientRequestLogin(this.player, login, password));
	}
	
	/**
	 * Requete qui permet de poser une bombe
	 * @return
	 */
	public boolean registerRequestActionPutBomb(AgentAction action) {
		return this.client.getClientService().registerRequest("REQUEST_ACTION_PUT_BOMB", new ClientRequestActionPutBomb(this.player, action));
	}
	
	/**
	 * Requete qui permet de déplacer un agent dans une direction
	 * @return
	 */
	public boolean registerRequestMoveBomberman(AgentMove move) {
		return this.client.getClientService().registerRequest("REQUEST_MOVE_AGENT", new ClientRequestMoveBomberman(this.player, move));
	}
	
	/**
	 * @return the client
	 */
	public Client<Player> getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client<Player> client) {
		this.client = client;
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @param player the player to set
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * @return the controllerBombermanGame
	 */
	public ControllerBombermanGame getControllerBombermanGame() {
		return controllerBombermanGame;
	}

	/**
	 * @param controllerBombermanGame the controllerBombermanGame to set
	 */
	public void setControllerBombermanGame(ControllerBombermanGame controllerBombermanGame) {
		this.controllerBombermanGame = controllerBombermanGame;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((controllerBombermanGame == null) ? 0 : controllerBombermanGame.hashCode());
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientBombermanGame other = (ClientBombermanGame) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (controllerBombermanGame == null) {
			if (other.controllerBombermanGame != null)
				return false;
		} else if (!controllerBombermanGame.equals(other.controllerBombermanGame))
			return false;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClientBombermanGame [client=" + client + ", player=" + player + ", controllerBombermanGame="
				+ controllerBombermanGame + "]";
	}
}
