package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de notifier le joueur de sa connexion
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyChat implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyChat.class);
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Notify Chat : " + player);
		String message = player.getMessages().get(0);
		LOG.debug("Message recu de serveur :" + message);
		
		if(ClientBombermanGame.getInstance().getControllerBombermanGame().getViewBomberman() != null) {
			ClientBombermanGame.getInstance().getControllerBombermanGame().getViewBomberman().writeMessage(message);	
		}
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}

