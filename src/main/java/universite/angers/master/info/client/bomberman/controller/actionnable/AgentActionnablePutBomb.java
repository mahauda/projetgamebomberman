package universite.angers.master.info.client.bomberman.controller.actionnable;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.client.ClientBombermanGame;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;

/**
 * Poser une bombe par un agent. En l'occurence le bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentActionnablePutBomb extends AgentActionnable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(AgentActionnablePutBomb.class);

	private Player player;

	@Override
	public boolean isLegalAction(Agent agent, AgentAction action) {
		LOG.debug("Is legal action put bomb agent : " + agent);
		LOG.debug("Is legal action put bomb action : " + action);
		
		if(agent == null) return false;
		if(action == null) return false;
		
		//On enregistre la demande de poser une bombe
		ClientBombermanGame.getInstance().registerRequestActionPutBomb(action);
				
		//On attend de recevoir la réponse du serveur
		ClientBombermanGame.getInstance().getClient().waitFinish();
				
		//On récupère la réponse contenu dans le joueur
		this.player = ClientBombermanGame.getInstance().getPlayer();
		LOG.debug("Player put a bomb : " + this.player);
		
		return this.player.isRecord();
	}

	@Override
	public void doAction(Agent agent, AgentAction action) {
		LOG.debug("Put bomb agent : " + agent);
		LOG.debug("Put bomb action : " + action);
		
		if(agent == null) return;
		if(action == null) return;
		
//		//On ajoute les bombes
//		for(InfoBomb bomb : this.player.getBombs()) {
//			LOG.debug("Bomb : " + bomb);
//			Map.getInstance().getStart_Bombs().add(bomb);
//		}
	}
}
