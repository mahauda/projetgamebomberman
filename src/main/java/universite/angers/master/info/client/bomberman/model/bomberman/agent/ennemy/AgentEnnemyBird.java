package universite.angers.master.info.client.bomberman.model.bomberman.agent.ennemy;

import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;

/**
 * Source : https://bomberman.fandom.com/wiki/Bird_(Bomberman_%2793) Bird moves
 * quickly and erratically. It can fly freely over wall
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentEnnemyBird extends AgentEnnemy {

	private static final long serialVersionUID = 1L;

	public AgentEnnemyBird(String identifiant, String name, int x, int y, AgentMove move, char type, ColorAgent color,
			boolean isInvincible, boolean isSick) {
		super(identifiant, name, x, y, move, type, color, isInvincible, isSick);
	}

	public AgentEnnemyBird(InfoAgent infoAgent) {
		super(infoAgent);
	}
}
