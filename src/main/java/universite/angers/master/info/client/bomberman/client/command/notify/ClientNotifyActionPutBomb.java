package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.controller.actionnable.InfoBomb;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de mettre à jour les bombes posés
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyActionPutBomb implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyActionPutBomb.class);

	@Override
	public boolean send(Player player) {
		LOG.debug("Notify action put bombe player : " + player);

		// On ajoute les bombes
		for (InfoBomb bomb : player.getBombs()) {
			LOG.debug("Bomb : " + bomb);
			Map.getInstance().getStart_Bombs().add(bomb);
		}

		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
