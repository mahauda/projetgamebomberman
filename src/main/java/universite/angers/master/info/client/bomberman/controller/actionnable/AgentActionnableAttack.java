package universite.angers.master.info.client.bomberman.controller.actionnable;

import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;

/**
 * Attaquer un autre agent. En l'occurence l'actuel bomberman manipulé dans le
 * plateau
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentActionnableAttack extends AgentActionnable {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean isLegalAction(Agent agent, AgentAction action) {
		return true;
	}

	@Override
	public void doAction(Agent agent, AgentAction action) {
		//Traitement effectué coté serveur
	}
}
