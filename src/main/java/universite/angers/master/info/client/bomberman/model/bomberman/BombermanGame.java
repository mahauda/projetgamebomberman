package universite.angers.master.info.client.bomberman.model.bomberman;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.model.Game;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.factory.AgentFactory;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.factory.AgentFactoryProvider;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.kind.AgentKind;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;

/**
 * Classe qui hérite Game et implémente les régles du jeu Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class BombermanGame extends Game {

	private static final Logger LOG = Logger.getLogger(BombermanGame.class);
	private static final long serialVersionUID = 1L;

	public BombermanGame(int maxturn, long time) {
		super(maxturn, time);
	}

	@Override
	public void initializeGame() {
		try {
			// On charge les instances selon le fichier
			Map.getInstance().load();
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return;
		}

		// Si la map n'est par chargé, on ne peut pas initialiser le jeu
		if (Map.getInstance().getStart_Agents().isEmpty())
			return;

		// On crée les différents agents avec le DP Factory
		for (InfoAgent infoAgent : Map.getInstance().getStart_Agents()) {
			AgentFactory agentFactory = AgentFactoryProvider.getAgentFactory(infoAgent.getType());
			Agent agent = agentFactory.createAgent(infoAgent);

			if (agent != null)
				Map.getInstance().getAgents().add(agent);

			if (agent instanceof AgentKind)
				Map.getInstance().getAgentsKind().add((AgentKind) agent);

			if (agent instanceof AgentEnnemy)
				Map.getInstance().getAgentsEnnemy().add((AgentEnnemy) agent);
		}

		// Pour l'instant on prend le premier bomberman
		Map.getInstance().setCurrentAgentKind(Map.getInstance().getAgentsKind().get(0));

		// On change le tableau de la map avec cette nouvelle liste d'agent
		// afin de les manipuler par référence
		Map.getInstance().setStart_Agents(new ArrayList<>(Map.getInstance().getAgents()));
	}
}
