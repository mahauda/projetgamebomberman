package universite.angers.master.info.client.bomberman.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.ArrayList;
import javax.swing.JPanel;
import universite.angers.master.info.client.bomberman.controller.actionnable.InfoBomb;
import universite.angers.master.info.client.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.InfoItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ItemType;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;

/** 
 * Classe qui permet de charger d'afficher le panneau du jeu à partir d'une carte et de listes d'agents avec leurs positions.
 * Inspiré du code de Kevin Balavoine et Victor Lelu--Ribaimont.
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class PanelBomberman extends JPanel {

	private static final long serialVersionUID = 1L;
	
	protected Color wallColor=Color.GRAY;
	protected Color brokable_walls_Color=Color.lightGray;
	protected Color ground_Color= new Color(50,175,50);

	private int taille_x;
	private int taille_y;

	private float[] contraste = { 0, 0, 0, 1.0f };
	private float[] invincible = { 200, 200, 200, 1.0f };
	private float[] skull = { 0.5f, 0.5f, 0.5f, 0.75f };

	private boolean walls[][];
	private boolean breakable_walls[][];
	private ArrayList<InfoAgent> listInfoAgents;
	private ArrayList<InfoItem> listInfoItems;
	private ArrayList<InfoBomb> listInfoBombs;

	int cpt;

	public PanelBomberman() {
		this.walls = Map.getInstance().getWalls();
		this.breakable_walls = Map.getInstance().getStart_brokable_walls();
		this.listInfoAgents = Map.getInstance().getStart_Agents();	
		this.listInfoItems = Map.getInstance().getStart_Items();
		this.listInfoBombs = Map.getInstance().getStart_Bombs();
	}

	@Override
	public void paint(Graphics g) {
	
		int fen_x = getSize().width;
		int fen_y = getSize().height;

		g.setColor(ground_Color);
		g.fillRect(0, 0,fen_x,fen_y);

		double stepx=fen_x/(double)taille_x;
		double stepy=fen_y/(double)taille_y;
		double position_x=0;

		taille_x = Map.getInstance().getSize_x();
		taille_y = Map.getInstance().getSize_y();
		
		for(int x=0; x<taille_x; x++) {
			
			double position_y = 0 ;
			
			for(int y=0; y<taille_y; y++) {
				
				if (this.walls[x][y]){

					try {
						//Image img = ImageIO.read(new File("./image/wall.png"))
						Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/wall.png"));
						g.drawImage(img, (int)position_x, (int)position_y, (int)stepx, (int)stepy, this);

	
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				else if (this.breakable_walls[x][y]){

					try {
						//Image img = ImageIO.read(new File("./image/brique_2.png"))
						Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/brique_2.png"));
						g.drawImage(img, (int)position_x, (int)position_y, (int)stepx, (int)stepy, this);
					} catch (IOException e) {
						e.printStackTrace();
					}

				}else {
					try {
						//Image img = ImageIO.read(new File("./image/grass.png"))
						Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/grass.png"));
						g.drawImage(img, (int)position_x, (int)position_y, (int)stepx, (int)stepy, this);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				position_y+=stepy;				
			}
			position_x+=stepx;
		}

		//On dessine affiche les bonus
		for(int i = 0; i < listInfoItems.size(); i++){
			dessine_Items(g, listInfoItems.get(i));	
		}

		//On affiche les bombes
		for (int j = 0 ; j < listInfoBombs.size() ; j++) {
			dessine_Bomb(g, listInfoBombs.get(j));
		}

		//On affiche les agents
		for(int i = 0; i < listInfoAgents.size(); i++){
			dessine_Agent(g,listInfoAgents.get(i));	
		}

		cpt++;
	}

	/**
	 * Méthode qui permet d'afficher un agent sur le plateau
	 * @param g le plateau
	 * @param infoAgent l'agent
	 */
	private void dessine_Agent(Graphics g, InfoAgent infoAgent) {
		if(g == null) return;
		if(infoAgent == null) return;
		
		int fen_x = getSize().width;
		int fen_y = getSize().height;

		double stepx = fen_x/(double)taille_x;
		double stepy = fen_y/(double)taille_y;

		int px = infoAgent.getX();
		int py = infoAgent.getY();

		double pos_x=px*stepx;
		double pos_y=py*stepy;

		AgentMove agentAction = infoAgent.getAgentMove();
		if(agentAction == null) return;

		int direction = 0;
		
		//On récupère la direction
		if(agentAction == AgentMove.MOVE_UP) {
			direction = 0;
		} else if(agentAction == AgentMove.MOVE_DOWN) {
			direction = 1;
		} else if(agentAction == AgentMove.MOVE_RIGHT) {
			direction = 2;
		} else if(agentAction == AgentMove.MOVE_LEFT) {
			direction = 3;			
		} else {
			direction = 4;
		}
		
		BufferedImage img = null;
		
		//On affiche l'agent selon la direction où il va
		try {
			if(infoAgent.getType() == 'R') {
				//img = ImageIO.read(new File("./image/" + infoAgent.getType() + direction + this.cpt % 2 + ".png"))
				img = ImageIO.read(this.getClass().getResourceAsStream("/image/"+ infoAgent.getType() + direction + this.cpt % 2 + ".png"));
			}else {
				//img = ImageIO.read(new File("./image/" + infoAgent.getType() + direction + this.cpt % 3 + ".png"))
				img = ImageIO.read(this.getClass().getResourceAsStream("/image/"+ infoAgent.getType() + direction + this.cpt % 3 + ".png"));
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}			

		
		//On affiche l'agent selon une couleur
		float [] scales = new float[]{1 ,1, 1, 1.0f };

		if(infoAgent.getColor()!= null) {
			
			switch(infoAgent.getColor()) {
				case ROUGE :
					scales = new float[]{3 ,0.75f, 0.75f, 1.0f };
					break;
				case VERT :
					scales = new float[]{0.75f ,3, 0.75f, 1.0f };
					break;
				case BLEU :
					scales = new float[]{0.75f ,0.75f, 3, 1.0f };
					break;
				case JAUNE :
					scales = new float[]{3 ,3, 0.75f, 1.0f };
					break;
				case BLANC :
					scales = new float[]{2 ,2, 2, 1.0f };
					break;
				case DEFAULT :
					scales = new float[]{1 ,1, 1, 1.0f };
					break;
				default:
					scales = new float[]{1 ,1, 1, 1.0f };
					break;	
			}
		}
		
		//Si l'agent est invisible on contraste l'agent
		if (infoAgent.isInvincible() & cpt % 2 == 0)
			contraste = invincible;
		else contraste = new float[]{ 0, 0, 0, 1.0f };
		
		//Si l'agent est malade on contraste l'agent
		if (infoAgent.isSick() & cpt % 2 == 0)
			scales = skull;
		
		RescaleOp op = new RescaleOp(scales, contraste, null);
		img = op.filter( img, null);
		
		if(img != null) {
			g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
		}
	}

	/**
	 * Méthode qui permet d'afficher un bonus sur le plateau
	 * @param g le plateau
	 * @param item le bonus
	 */
	private void dessine_Items(Graphics g, InfoItem item) {
		int fen_x = getSize().width;
		int fen_y = getSize().height;

		double stepx = fen_x/(double)taille_x;
		double stepy = fen_y/(double)taille_y;

		int px = item.getX();
		int py = item.getY();

		double pos_x=px*stepx;
		double pos_y=py*stepy;

		//FIRE
		
		if (item.getType() == ItemType.FIRE_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_FireUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_FireUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.FIRE_FULL_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_FireFullUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_FireFullUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.FIRE_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_FireDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_FireDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.FIRE_FULL_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_FireFullDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_FireFullDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//BOMB
		
		if (item.getType() == ItemType.BOMB_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_BombUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_BombUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.BOMB_FULL_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_BombFullUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_BombFullUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.BOMB_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_BombDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_BombDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.BOMB_FULL_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_BombFullDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_BombFullDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//SPEED
		
		if (item.getType() == ItemType.SPEED_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_SpeedUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_SpeedUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.SPEED_FULL_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_SpeedFullUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_SpeedFullUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.SPEED_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_SpeedDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_SpeedDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.SPEED_FULL_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_SpeedFullDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_SpeedFullDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//LIFE
		
		if (item.getType() == ItemType.LIFE_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_LifeUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_LifeUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.LIFE_FULL_UP) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_LifeFullUp.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_LifeFullUp.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.LIFE_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_LifeDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_LifeDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (item.getType() == ItemType.LIFE_FULL_DOWN) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_LifeFullDown.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_LifeFullDown.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//Invisible
		
		if (item.getType() == ItemType.FIRE_SUIT) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_FireSuit.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_FireSuit.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//Malade
		
		if (item.getType() == ItemType.SKULL) {
			try {
				//Image img = ImageIO.read(new File("./image/Item_Skull.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Item_Skull.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Méthode qui permet d'afficher une bombe sur le plateau
	 * @param g le plateau
	 * @param bomb la bombe
	 */
	private void dessine_Bomb(Graphics g, InfoBomb bomb) {
		int fen_x = getSize().width;
		int fen_y = getSize().height;

		double stepx = fen_x/(double)taille_x;
		double stepy = fen_y/(double)taille_y;

		int px = bomb.getX();
		int py = bomb.getY();

		double pos_x=px*stepx;
		double pos_y=py*stepy;

		if (bomb.getStateBomb() == StateBomb.Step1) {

			try {
				//Image img = ImageIO.read(new File("./image/Bomb_0.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Bomb_0.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if (bomb.getStateBomb() == StateBomb.Step2) {

			try {
				//Image img = ImageIO.read(new File("./image/Bomb_1_jaune.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Bomb_1_jaune.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if (bomb.getStateBomb() == StateBomb.Step3) {

			try {
				//Image img = ImageIO.read(new File("./image/Bomb_2_rouge.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Bomb_2_rouge.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		else if (bomb.getStateBomb() == StateBomb.Boom) {

			try {
				//Image img = ImageIO.read(new File("./image/Range_CENTRE.png"))
				Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_CENTRE.png"));
				g.drawImage(img, (int)pos_x, (int)pos_y, (int)stepx, (int)stepy, this);
			} catch (IOException e) {
				e.printStackTrace();
			}

			int range = bomb.getRange();

			for (int i = 1 ; i <= range; i++){

				if(py+i < Map.getInstance().getSize_y()) {
					if(i == range ) {
						try {
							//Image img = ImageIO.read(new File("./image/Range_SOUTH_Fin.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_SOUTH_Fin.png"));
							g.drawImage(img, (int)pos_x, (int)(pos_y + (stepy*i)), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}else {
						try {
							//Image img = ImageIO.read(new File("./image/Range_SOUTH.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_SOUTH.png"));
							g.drawImage(img, (int)pos_x, (int)(pos_y + (stepy*i)), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

				if(py-i >= 0) {
					if(i == range) {
						try {
							//Image img = ImageIO.read(new File("./image/Range_NORTH_Fin.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_NORTH_Fin.png"));
							g.drawImage(img, (int)pos_x, (int)(pos_y - (stepy*i)), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}else {
						try {
							//Image img = ImageIO.read(new File("./image/Range_NORTH.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_NORTH.png"));
							g.drawImage(img, (int)pos_x, (int)(pos_y - (stepy*i)), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

				if(px+i < Map.getInstance().getSize_x()) {
					if( i == range ) {
						try {
							//Image img = ImageIO.read(new File("./image/Range_EAST_Fin.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_EAST_Fin.png"));
							g.drawImage(img, (int)(pos_x + (stepy*i)), (int)(pos_y), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}else {
						try {
							//Image img = ImageIO.read(new File("./image/Range_EAST.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_EAST.png"));
							g.drawImage(img, (int)(pos_x + (stepy*i)), (int)(pos_y), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

				if(px-i >= 0) {
					if( i == range) {
						try {
							//Image img = ImageIO.read(new File("./image/Range_WEST_Fin.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_WEST_Fin.png"));
							g.drawImage(img, (int)(pos_x - (stepy*i)), (int)(pos_y), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}else {
						try {
							//Image img = ImageIO.read(new File("./image/Range_WEST.png"))
							Image img = ImageIO.read(this.getClass().getResourceAsStream("/image/Range_WEST.png"));
							g.drawImage(img, (int)(pos_x - (stepy*i)), (int)(pos_y), (int)stepx, (int)stepy, this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Méthode qui permet de rafraichir le plateau selon les nouvelles données
	 * @param breakable_walls
	 * @param listInfoAgents
	 * @param listInfoItems
	 * @param listInfoBombs
	 */
	public void setInfoGame(boolean[][] breakable_walls, ArrayList<InfoAgent> listInfoAgents, ArrayList<InfoItem> listInfoItems, ArrayList<InfoBomb> listInfoBombs) {	
		this.breakable_walls = breakable_walls;	
		this.listInfoAgents = listInfoAgents;
		this.listInfoItems = listInfoItems;
		this.listInfoBombs = listInfoBombs;
	}
}