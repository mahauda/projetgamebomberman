package universite.angers.master.info.client.bomberman.client;

import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.client.bomberman.model.Player;

/**
 * Classe qui permet de récupérer le nom de la commande dans un objet "Player"
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientConverterCommandName implements Convertable<String, Player> {

	@Override
	public String convert(Player message) {
		return message.getCommand();
	}
}
