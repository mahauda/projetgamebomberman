package universite.angers.master.info.client.bomberman.model.bomberman.agent;

import java.io.Serializable;

import universite.angers.master.info.client.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.client.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.client.bomberman.model.bomberman.ColorAgent;

/**
 * Les caractéristiques de chaque agent
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class InfoAgent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * L'identifiant de l'agent
	 */
	protected String identifiant;
	
	/**
	 * Le nom de l'agent
	 */
	protected String name;
	
	/**
	 * La coordonnée de x où se trouve l'agent sur le plateau
	 */
	protected int x;
	
	/**
	 * La coordonnée de y où se trouve l'agent sur le plateau
	 */
	protected int y;
	
	/**
	 * La position de l'agent
	 */
	protected AgentMove agentMove;
	
	/**
	 * La couleur de l'agent
	 */
	protected ColorAgent color;
	
	/**
	 * Le type de l'agent ?
	 */
	protected char type;
	
	/**
	 * Est-il invisible ?
	 */
	protected boolean isInvincible;
	
	/**
	 * Est-il malade ?
	 */
	protected boolean isSick;
	
	/**
	 * L'état de la bombe lorsque l'agent pose la bombe
	 */
	protected StateBomb stateBomb;
	
	public InfoAgent() {
		this("", "", 0, 0, null, Character.MIN_VALUE, null, false, false, StateBomb.Step1);
	}
	
	public InfoAgent(String identifiant, String name, int x, int y, AgentMove agentMove, char type, ColorAgent color, 
			boolean isInvincible, boolean isSick, StateBomb stateBomb) {
		this.identifiant = identifiant;
		this.name = name;
		this.x=x;
		this.y=y;
		this.agentMove = agentMove;
		this.color = color;
		this.type = type;
		this.isInvincible = isInvincible;
		this.isSick = isSick;
		this.stateBomb = stateBomb;
	}
	
	/**
	 * @return the identifiant
	 */
	public String getIdentifiant() {
		return identifiant;
	}

	/**
	 * @param identifiant the identifiant to set
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the agentMove
	 */
	public AgentMove getAgentMove() {
		return agentMove;
	}

	/**
	 * @param agentMove the agentMove to set
	 */
	public void setAgentMove(AgentMove agentMove) {
		this.agentMove = agentMove;
	}

	/**
	 * @return the color
	 */
	public ColorAgent getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(ColorAgent color) {
		this.color = color;
	}

	/**
	 * @return the type
	 */
	public char getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(char type) {
		this.type = type;
	}

	/**
	 * @return the isInvincible
	 */
	public boolean isInvincible() {
		return isInvincible;
	}

	/**
	 * @param isInvincible the isInvincible to set
	 */
	public void setInvincible(boolean isInvincible) {
		this.isInvincible = isInvincible;
	}

	/**
	 * @return the isSick
	 */
	public boolean isSick() {
		return isSick;
	}

	/**
	 * @param isSick the isSick to set
	 */
	public void setSick(boolean isSick) {
		this.isSick = isSick;
	}

	/**
	 * @return the stateBomb
	 */
	public StateBomb getStateBomb() {
		return stateBomb;
	}

	/**
	 * @param stateBomb the stateBomb to set
	 */
	public void setStateBomb(StateBomb stateBomb) {
		this.stateBomb = stateBomb;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentMove == null) ? 0 : agentMove.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + (isInvincible ? 1231 : 1237);
		result = prime * result + (isSick ? 1231 : 1237);
		result = prime * result + ((stateBomb == null) ? 0 : stateBomb.hashCode());
		result = prime * result + type;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoAgent other = (InfoAgent) obj;
		if (agentMove != other.agentMove)
			return false;
		if (color != other.color)
			return false;
		if (isInvincible != other.isInvincible)
			return false;
		if (isSick != other.isSick)
			return false;
		if (stateBomb != other.stateBomb)
			return false;
		if (type != other.type)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InfoAgent [x=" + x + ", y=" + y + ", agentMove=" + agentMove + ", color=" + color + ", type=" + type
				+ ", isInvincible=" + isInvincible + ", isSick=" + isSick + ", stateBomb=" + stateBomb + "]";
	}
}
	