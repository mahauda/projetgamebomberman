package universite.angers.master.info.client.bomberman.client.command.notify;

import org.apache.log4j.Logger;
import universite.angers.master.info.client.bomberman.model.Player;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.client.bomberman.model.bomberman.agent.ItemType;
import universite.angers.master.info.client.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de mettre à jour le nombre de vie du bomberman
 * en fonction des attaques des ennemis
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientNotifyAttackBomberman implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ClientNotifyAttackBomberman.class);
	
	@Override
	public boolean send(Player player) {
		LOG.debug("Notify attack bomberman player : " + player);
		
		//On update les caractéristiques du Bomberman
		Agent bomberman = player.getBomberman();
		LOG.debug("Bomberman : " + bomberman);
		
		CapacityItem lifeBombServeur = bomberman.getCapacityItem(ItemType.LIFE);
		LOG.debug("Life bomberman serveur : " + lifeBombServeur);
		
		CapacityItem lifeBombClient = Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.LIFE);
		LOG.debug("Life bomberman client : " + lifeBombClient);
		
		//On met jour le nombre de vie du client : mort ou toujours vivant
		lifeBombClient.setCapacityActualItem(lifeBombServeur.getCapacityActualItem());
		
		return true;
	}

	@Override
	public Player receive(Object arg) {
		return null;
	}
}
