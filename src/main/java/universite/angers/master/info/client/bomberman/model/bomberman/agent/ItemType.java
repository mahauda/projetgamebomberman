package universite.angers.master.info.client.bomberman.model.bomberman.agent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Enumération des différents items
 * - Bonus et malus porté sur l'explosion des bombes
 * - Bonus et malus sur le nombre que peut poser simultanément un agent bomberman
 * - Bonus et malus sur le nombre de vie
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum ItemType {
	FIRE, FIRE_UP, FIRE_DOWN, FIRE_FULL_UP, FIRE_FULL_DOWN, //bonus porté explosion
	BOMB, BOMB_UP, BOMB_DOWN, BOMB_FULL_UP, BOMB_FULL_DOWN, //bonus nombre de bombe
	LIFE, LIFE_UP, LIFE_DOWN, LIFE_FULL_UP, LIFE_FULL_DOWN, //bonus vie
	SPEED, SPEED_UP, SPEED_DOWN, SPEED_FULL_UP, SPEED_FULL_DOWN, //bonus vitesse
	FIRE_SUIT, //invisibles
	SKULL, //atteint d'une maladie
	POINT, //nombre de point
	ATTACK;
	
	public static ItemType[] valuesRemove() {
		return new ItemType[] {FIRE, BOMB, LIFE, SPEED, POINT, ATTACK};
	}
	
	public static List<ItemType> valuesUse() {
		List<ItemType> itemTypes = new ArrayList<>(Arrays.asList(ItemType.values()));
		itemTypes.removeAll(Arrays.asList(ItemType.valuesRemove()));
		return itemTypes;
	}
}