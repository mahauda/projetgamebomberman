package universite.angers.master.info.client.bomberman.controller;

import universite.angers.master.info.client.bomberman.model.Game;

/**
 * Interface qui permet d'assurer les contrôles du jeu quand des actions ont été effectuées par
 * l’utilisateur dans la Vue (par exemple lorsque l’utilisateur clique sur un bouton)
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class Controller {
	
	/**
	 * Le jeu à controller
	 */
	protected Game game;
	
	public Controller(Game game) {
		this.game = game;
	}
	
	/**
	 * Méthode qui initialise la vue du jeu
	 */
	public abstract void init();
	
	/**
	 * Méthode qui permet de se connecter au jeu
	 */
	public abstract void login(String login, String password);
	
	/**
	 * Méthode qui permet de se déconnecter du jeu
	 */
	public abstract void logout();
	
	/**
	 * Méthode qui initialise le jeu
	 */
	public abstract void start();
	
	/**
	 * Méthode qui effectue un tour dans le jeu
	 */
	public abstract void step();
	
	/**
	 * Méthode qui démarre le jeu tant qu'il y a des tours à effectuer
	 */
	public abstract void run();
	
	/**
	 * Méthode qui met en pause le jeu
	 */
	public abstract void stop();
	
	/**
	 * Méthode qui modifie le temps d'execution entre chaque tour de jeu.
	 */
	public abstract void setTime(long time);

	/**
	 * Fermer la fenetre du jeu si active
	 * @return
	 */
	public abstract void close();
	
	/**
	 * Ouvrir le jeu
	 */
	public abstract void open();

	/**
	 * Affiche une vue lorsque la partie est terminé
	 * - Soit gagné et passer au niveau suivant
	 * - Soit perdu et afficher gameover
	 */
	public abstract void displayGameFinish();

	/**
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * @param game the game to set
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((game == null) ? 0 : game.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Controller other = (Controller) obj;
		if (game == null) {
			if (other.game != null)
				return false;
		} else if (!game.equals(other.game))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Controller [game=" + game + "]";
	}
}
