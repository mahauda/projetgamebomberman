package universite.angers.master.info.client.bomberman.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * Vue qui permet de demander les identifiants au joueur avant de jouer
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ViewLogin extends JFrame {

	private static final long serialVersionUID = 1L;

	private JFrame window;
	private JLabel labLogin;
	private JLabel labPassword;
	private JTextField fieldLogin;
	private JPasswordField fieldPassword;
	private JButton butValidate;
	private JButton butCancel;

	private String login;
	private String password;
	private boolean validLogin;

	public void display() {
		this.validLogin = false;
		this.window = new JFrame("Veuillez entrer vos identifiants");

		this.labLogin = new JLabel("Login");
		this.labPassword = new JLabel("Mot de passe");
		this.fieldLogin = new JTextField();
		this.fieldPassword = new JPasswordField();
		this.butCancel = new JButton("Annuler");
		this.butValidate = new JButton("Valider");

		this.labLogin.setBounds(80, 30, 100, 30);
		this.fieldLogin.setBounds(300, 30, 230, 30);

		this.labPassword.setBounds(80, 80, 100, 30);
		this.fieldPassword.setBounds(300, 80, 230, 30);

		this.butCancel.setBounds(300, 140, 100, 30);
		this.butValidate.setBounds(430, 140, 100, 30);

		this.butValidate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Validate");

				// Si le login n'est pas renseigné alors on ne valide pas et on
				// avertit l'utilisateur
				if (fieldLogin.getText().isEmpty()) {
					
					Object[] optionsFin = {"OK"};	
					
					JOptionPane.showOptionDialog(window,
							"Veuillez renseigner le login !",
							"Login non renseigné",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE,
							null,     
							optionsFin,  
							optionsFin[0]);

					return;
				}

				// Si le password n'est pas renseigné alors on ne valide pas
				if (fieldPassword.getPassword().length == 0) {
					
					Object[] optionsFin = {"OK"};	
					
					JOptionPane.showOptionDialog(window,
							"Veuillez renseigner un mot de passe valide !",
							"Mot de passe non renseigné",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE,
							null,     
							optionsFin,  
							optionsFin[0]);
					
					return;
				}

				// A la fin on close

				validLogin = true;
				
				login = fieldLogin.getText();
				System.out.println("login :" + login);
				
				password = String.valueOf(fieldPassword.getPassword());
				System.out.println("password : " + password);
				
				window.setVisible(false);
				window.dispose();
				dispose();
			}
		});


		this.butCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Cancel");
				validLogin = false;
				
				window.setVisible(false);
				window.dispose();
				dispose();
			}
		});
		
		this.window.add(this.labLogin);
		this.window.add(this.fieldLogin);
		this.window.add(this.labPassword);
		this.window.add(this.fieldPassword);
		this.window.add(this.butCancel);
		this.window.add(this.butValidate);

		this.window.setSize(600, 200);
		this.window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.window.setLayout(null);
		this.window.setVisible(true);
		this.window.setLocationRelativeTo(null); //permet de centrer la fenetre au centre de l'écran
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * @return the window
	 */
	public JFrame getWindow() {
		return window;
	}

	/**
	 * @param window the window to set
	 */
	public void setWindow(JFrame window) {
		this.window = window;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the validLogin
	 */
	public boolean isValidLogin() {
		return validLogin;
	}

	/**
	 * @param validLogin the validLogin to set
	 */
	public void setValidLogin(boolean validLogin) {
		this.validLogin = validLogin;
	}
}